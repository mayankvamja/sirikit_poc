//
//  GlucoseDataManager.swift
//  GlucoseStatsKit
//
//  Created by Mayank Vamja on 17/06/21.
//

import Foundation
import Intents

public struct GlucoseData: Codable {
    public let id: UUID
    public let value: Double
    
    public init(from value: Double) {
        self.id = UUID()
        self.value = value
    }
}

public enum GDError: Error {
    case noRecords
    case canNotAdd
    case canNotRemove
    case unknown
}

public class GlucoseDataManager {
    
    public static let shared = GlucoseDataManager()
        
    private let filePath: URL? = {
//        let groupURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "")
//        if groupURL == nil {
            let documentsDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            return documentsDir.appendingPathComponent("glucose-data.plist")
//        }
//        
//        return groupURL!.appendingPathComponent("glucose-data.plist")
    }()
    
    private let encoder = PropertyListEncoder()
    private let decoder = PropertyListDecoder()
    
    public var data: [GlucoseData] = []
    
    init() {
        fetchAll()
    }
    
    /// method create glucose readings user activity
    public func glucoseReadingUserActivity() -> NSUserActivity {
        let activity = NSUserActivity(activityType: "com.example.PoCSiriKit.glucoseReadings")
        activity.persistentIdentifier = NSUserActivityPersistentIdentifier("com.example.PoCSiriKit.glucoseReadings")
        
        activity.isEligibleForSearch = true
        activity.isEligibleForPrediction = true
        
        // Title
        activity.title = "My last glucose reading"
        
        // Suggested Phrase
        activity.suggestedInvocationPhrase = "My glucose reading!"
        
        return activity
    }
    
    /// method to dontate an intent
    public func donateGlucoseReadingIntent() -> INIntent {
        let intent = GlucoseReadingIntent()
        let interaction = INInteraction(intent: intent, response: nil)
        
        interaction.donate { (error) in
            if let _error = error {
                print("\n\(#file) \nFailed to dontate intent with error: \(_error)\n")
            }
        }
        
        return intent
    }
    
    public func add(value: GlucoseData) -> (success: Bool, error: Error?) {

        var valuesToEncode = data
        valuesToEncode.append(value)
        
        if let url = filePath,
           let data = try? encoder.encode(valuesToEncode),
           ((try? data.write(to: url)) != nil) {
            debugPrint("\(value) added")
            self.data.append(value)
            return (success: true, error: nil)
        }
        
        return (success: false, error: GDError.canNotAdd)
    }
    
    public func remove(index: Int) -> (success: Bool, error: Error?) {
        
        var valuesToEncode = data
        valuesToEncode.remove(at: index)
        
        if let url = filePath,
           let data = try? encoder.encode(valuesToEncode),
           ((try? data.write(to: url)) != nil) {
            
            debugPrint("Data at \(index) added")
            self.data.remove(at: index)
            return (success: true, error: nil)
            
        }
        
        return (success: false, error: GDError.canNotRemove)
    }
    
    public func fetchAll() {
        if let url = filePath,
           let content = try? Data(contentsOf: url),
           let _data = try? decoder.decode([GlucoseData].self, from: content) {
            self.data = _data
        }
    }
    
    public func fetchLast() -> (value: GlucoseData?, error: Error?) {
        
        return (value: .init(from: Double.random(in: 70 ... 150)), error: nil)
        
//        guard let url = filePath else {
//            debugPrint("filePath nil")
//            return (value: nil, error: GDError.unknown)
//        }
//
//        guard let content = FileManager.default.contents(atPath: url.absoluteString) else {
//            debugPrint("no file content")
//            return (value: nil, error: GDError.unknown)
//        }
//
//        guard let _data = try? decoder.decode([GlucoseData].self, from: content),
//              let lastElement = _data.last else {
//            debugPrint("no records")
//            return (value: nil, error: GDError.noRecords)
//        }
//
//        return (value: lastElement, error: nil)
    }
    
    public func fetchLast(size numberOfRecords: Int) -> (value: [GlucoseData]?, error: Error?) {
        
        var array: [GlucoseData] = []
        for _ in 0 ..< Int.random(in: 1...numberOfRecords) {
            array.append(.init(from: Double.random(in: 70...150)))
        }
        
        return (value: array, error:  nil)
        
//        let value: [GlucoseData]?
//        let error: Error?
//
//        if let url = filePath,
//           let content = try? Data(contentsOf: url),
//           let _data = try? decoder.decode([GlucoseData].self, from: content),
//           _data.count > 0 {
//
//            let lastKElements = _data.suffix(numberOfRecords)
//            error = nil
//            value = Array(lastKElements)
//
//        } else {
//            error = GDError.noRecords
//            value = nil
//        }
//
//        return (value: value, error: error)
    }
}
