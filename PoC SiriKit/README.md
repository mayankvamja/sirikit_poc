- Enable Siri in Capabilities

- Create Intent definition file 
    - make it available to required targets
    - Generate public classes in shared target

- Add Intent extension ( and UI extension for UI )
- Add Intent handling in it
    - add NSExtensions -> IntentsSupported in info.plist of app, app-extension (also in UI extension)
    
-- Request siri permission (Only for System Intents)
-- Donate Intents for siri suggestions
