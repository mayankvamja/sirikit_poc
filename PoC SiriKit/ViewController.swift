//
//  ViewController.swift
//  PoC SiriKit
//
//  Created by Mayank Vamja on 16/06/21.
//

import UIKit

/// module with generated  Intent classes
import GlucoseStatsKit

import Intents
import IntentsUI

class ViewController: UIViewController {
    
    @IBOutlet var glucoseReadingStack: UIStackView!
    @IBOutlet var carbsStack: UIStackView!
    @IBOutlet var insulineStack: UIStackView!
    
    lazy private var addToSiriGlucoseReadingAction = UIAction { [unowned self] (_) in
        let intent = GlucoseReadingIntent()
        intent.suggestedInvocationPhrase = "Show My Glucose Reading"
        
        self.addVoiceShortcutToSiri(with: intent)
    }
    
    lazy private var addToSiriAddCarbsAction = UIAction { [unowned self] (_) in
        let intent = AddCarbsIntent()
        intent.suggestedInvocationPhrase = "Add 50 g Carbs"
        
        self.addVoiceShortcutToSiri(with: intent)
    }
    
    lazy private var addToSiriAddInsulineAction = UIAction { [unowned self] (_) in
        let intent = AddInsulinIntent()
        intent.suggestedInvocationPhrase = "Add One Unit Insulin"
        
        self.addVoiceShortcutToSiri(with: intent)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetchShortcuts()

        addSiriButton(in: glucoseReadingStack, with: addToSiriGlucoseReadingAction)
        addSiriButton(in: carbsStack, with: addToSiriAddCarbsAction)
        addSiriButton(in: insulineStack, with: addToSiriAddInsulineAction)
    }
    
    
    
    func addSiriButton(in view: UIStackView, with action: UIAction) {
        let button = INUIAddVoiceShortcutButton(style: .black)
        button.addAction(action, for: .touchUpInside)
        view.addArrangedSubview(button)
    }

    /*

    @IBAction func didTapAddToSiriWithParam() {
        
        let intent = GlucoseStatsIntent()
        intent.numberOfRecords = nil
        intent.suggestedInvocationPhrase = "My glucose measurements"

        addVoiceShortcutToSiri(with: intent)
    }
    */
    
    /// Fetch all shortcuts registered
    /*private func fetchShortcuts() {
        let center =  INVoiceShortcutCenter.shared
        
        center.getAllVoiceShortcuts { (data, error) in
            guard error == nil else {
                debugPrint("\n\(#file) \nFailed to getAllVoiceShortcuts")
                return
            }
            
            guard let shortcuts = data, shortcuts.count > 0 else {
                debugPrint("\n\(#file) \nNo VoiceShortcuts Available")
                return
            }

            DispatchQueue.global().async { [weak self] in
                shortcuts.forEach { (voiceShortcut) in
                    guard let intent = voiceShortcut.shortcut.intent else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self?.handleIntent(intent)
                    }
                    
                }
            }

        }
    }
    
    private func handleIntent(_ intent: INIntent) {
        switch intent {
        
        case is GlucoseReadingIntent:
            glucoseReadingStack.isHidden = true

        case is AddCarbsIntent:
            carbsStack.isHidden = true

        case is AddInsulinIntent:
            insulineStack.isHidden = true

        default:
            break
        }
    }*/
    
    func addVoiceShortcutToSiri(with intent: INIntent) {

        guard let shortcut = INShortcut(intent: intent) else {
            debugPrint("Failed to create INShortcut from \(intent.description)")
            return
        }
        
        let vc = INUIAddVoiceShortcutViewController(shortcut: shortcut)
        vc.delegate = self
        
        present(vc, animated: true, completion: nil)
    }
    
}

extension ViewController: INUIAddVoiceShortcutViewControllerDelegate {
    func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
        debugPrint("\(voiceShortcut?.shortcut.intent?.description ?? "Intent") addded VoiceShortcutToSiri")
        self.dismiss(animated: true)
    }
    
    func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        debugPrint("\(#file) cancelled VoiceShortcutToSiri")
        self.dismiss(animated: true)
    }
    
    
}
