//
//  GlucoseStatsIntentHandler.swift
//  GlucoseStatsIntent
//
//  Created by Mayank Vamja on 17/06/21.
//

import Foundation
import GlucoseStatsKit
import Intents

/// `GlucoseStats` Intent with parameter `numberOfRecords`
class GlucoseStatsIntentHandler: NSObject, GlucoseStatsIntentHandling {
    
    func handle(intent: GlucoseStatsIntent, completion: @escaping (GlucoseStatsIntentResponse) -> Void) {
        
        debugPrint("\(#file.split(separator: "/").last ?? "-"): \(#function)")
        let size = intent.numberOfRecords?.intValue ?? 1
        
        let result = GlucoseDataManager.shared.fetchLast(size: size)
        if let error = result.error {
            debugPrint("GlucoseReadingIntentHandler Failed with error: \(error)")
            completion(.failure(errorMessage: "Failed to read you last \(size) glucose readings"))
            return
        }
        
        
        let values = result.value!.map({ String(format: "%.2f", $0.value) }).joined(separator: ",")
        let stringValue: String
        
        if result.value!.count == size {
            stringValue = "Your last \(size) readings are \(values)"
        } else {
            stringValue = "Olny \(result.value!.count) records found instead \(size), and readings are \(values)"
        }
        
        
        completion(.success(successMessage: stringValue))
        return
        
    }
    
    func resolveNumberOfRecords(for intent: GlucoseStatsIntent, with completion: @escaping (GlucoseStatsNumberOfRecordsResolutionResult) -> Void) {
        
        debugPrint("\(#file.split(separator: "/").last ?? "-"): \(#function)")
        
        guard let size = intent.numberOfRecords?.intValue else {
            completion(.needsValue())
            return
        }
        
        guard size >= 1 else {
            completion(.unsupported(forReason: .lessThanMinimumValue))
            return
        }
        
        guard size <= 5 else {
            completion(.unsupported(forReason: .greaterThanMaximumValue))
            return
        }
        
        completion(.success(with: size))
        return
        
    }
    
    
}
