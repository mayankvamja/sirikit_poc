//
//  GlucoseReadingIntentHandler.swift
//  GlucoseStatsIntent
//
//  Created by Mayank Vamja on 17/06/21.
//

import GlucoseStatsKit
import Intents

/// `GlucoseReading` Intent
class GlucoseReadingIntentHandler: NSObject, GlucoseReadingIntentHandling {
    func handle(intent: GlucoseReadingIntent, completion: @escaping (GlucoseReadingIntentResponse) -> Void) {
        
        completion(.success(glucoseValues: ["110 mg/dL"]))
        return
    }
    
}
