//
//  IntentHandler.swift
//  GlucoseStatsIntent
//
//  Created by Mayank Vamja on 17/06/21.
//

import Intents
import GlucoseStatsKit

class AddCarbsIntentHandler: NSObject, AddCarbsIntentHandling {
    func handle(intent: AddCarbsIntent, completion: @escaping (AddCarbsIntentResponse) -> Void) {
        completion(.success(successMessage: "I have added 50 grams carbs to your record"))
    }
}

class AddInsulinIntentHandler: NSObject, AddInsulinIntentHandling {
    func handle(intent: AddInsulinIntent, completion: @escaping (AddInsulinIntentResponse) -> Void) {
        completion(.success(successMessage: "I have added 1 unit insulin to your record"))
    }
}

class IntentHandler: INExtension {
    
    override func handler(for intent: INIntent) -> Any {
        
        switch intent {
        
        case is GlucoseStatsIntent:
            return GlucoseStatsIntentHandler()
            
        case is GlucoseReadingIntent:
            return GlucoseReadingIntentHandler()
            
        case is AddCarbsIntent:
            return AddCarbsIntentHandler()
            
        case is AddInsulinIntent:
            return AddInsulinIntentHandler()
        default:
            return self
        }
        
    }
    
}
