//
//  IntentViewController.swift
//  GlucoseStatsIntentUI
//
//  Created by Mayank Vamja on 17/06/21.
//

import IntentsUI
import GlucoseStatsKit

class IntentViewController: UIViewController, INUIHostedViewControlling {
    
    @IBOutlet var imageView: UIImageView!
    
    private lazy var intentUIBundle = Bundle(identifier: "com.example.PoC-SiriKit.GlucoseStatsIntentUI")
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
        
    // MARK: - INUIHostedViewControlling
    
    // Prepare your view controller for the interaction to handle.
    func configureView(for parameters: Set<INParameter>, of interaction: INInteraction, interactiveBehavior: INUIInteractiveBehavior, context: INUIHostedViewContext, completion: @escaping (Bool, Set<INParameter>, CGSize) -> Void) {
                
        guard interaction.intentHandlingStatus == .success, let response = interaction.intentResponse else {
            completion(false, parameters, .zero)
            return
        }
        
        let maxWidth = self.extensionContext!.hostedViewMaximumAllowedSize.width
        
        switch response {
        
        case response as GlucoseReadingIntentResponse:
//            imageView.image = UIImage(contentsOfFile: Bundle(identifier: "com.example.PoC-SiriKit.GlucoseStatsIntentUI")!.path(forResource: "glucose.png", ofType: nil)!)
            imageView.image = UIImage(named: "GlucoseImage", in: intentUIBundle, with: nil)
            let height = (488 * maxWidth) / 1125
            let size = CGSize(width: maxWidth, height: height)
            // 1125 x 488
            
            completion(true, parameters, size)
            return
            
        case response as AddInsulinIntentResponse:
//            imageView.image = UIImage(contentsOfFile: Bundle(identifier: "com.example.PoC-SiriKit.GlucoseStatsIntentUI")!.path(forResource: "insulin.png", ofType: nil)!)
//            imageView.image = UIImage(named: "InsulinImage", in: intentUIBundle, with: nil)
            imageView.image = UIImage(systemName: "circle.fill")
            let height = (515 * maxWidth) / 1125
            let size = CGSize(width: maxWidth, height: height)
            // 1125 x 515
            
            completion(true, parameters, size)
            return
            
        case response as AddCarbsIntentResponse:
            print("-- AddCarbsIntentResponse IntentUI --")
//            imageView.image = UIImage(contentsOfFile: Bundle(identifier: "com.example.PoC-SiriKit.GlucoseStatsIntentUI")!.path(forResource: "carbs.png", ofType: nil)!)
            imageView.image = UIImage(named: "CarbsImage", in: intentUIBundle, with: nil)
            let height = (361 * maxWidth) / 1125
            let size = CGSize(width: maxWidth, height: height)
            // 1125 x 361
            
            completion(true, parameters, size)
            return
            
        
        default:
            completion(false, parameters, .zero)
            return
        }
        
    }
    
}

class GDResponseViewController: UIViewController {
    let label = UILabel(frame: .zero)
    
}
